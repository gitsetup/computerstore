//variables
const balanceElement = document.getElementById("balance");
const payElement = document.getElementById("pay");
const bankElement = document.getElementById("bank");
const workElement = document.getElementById("work");
const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const buyElement = document.getElementById("buy");
const footerElement = document.getElementById("footer");
const loanElement = document.getElementById("loan");
const loanBalance = document.getElementById("loanBalance");
const repayElement = document.getElementById("repayLoan");
const footerImage = document.getElementById("footer-img");
//general state
let laptops = [];
let balance = balanceElement.innerHTML;
let currentLoan = loanBalance.innerHTML;
let loanAmount = 0;
let pay = 0;

//Api url
const apiUrl = "https://noroff-komputer-store-api.herokuapp.com/computers";

//fetch Api
fetch(apiUrl)
  .then((resp) => resp.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToMenu(laptops));

//functions
//Banker
const handleLoan = () => {
  //if loan is 0
  if (loanAmount == 0 && balance !== 0) {
    loanAmount = prompt("Please enter your loan amount:");
    if (loanAmount <= balance * 2) {
      loanBalance.innerHTML = `${loanAmount}`;
      balance = parseInt(balance) + parseInt(loanAmount);
      balanceElement.innerHTML = balance;
      //if loan is not equal to 0, show the repay button
      if (loanAmount !== 0) {
        repayElement.style.visibility = "visible";
        console.log(balance);
        handleRepayLoan();
      } else {
        repayElement.style.visibility = "hidden";
      }
    } else {
      alert("cant loan more than the double of your balance account");
      handleLoan();
    }
  } else {
    if (balance == 0) {
      alert("TIME TO WORK!");
    } else {
      alert("Need to repay your loan before taking another one");
    }
  }
};

//work && pay
const handleWork = () => {
  //increase 100
  pay += 100;
  payElement.innerHTML = parseInt(pay);
};

//function to transfer money
const handleTransfersMoney = () => {
  payElement.innerHTML = "";
  let currentTransfer = 0;
  let tax = (pay / 100) * 10;
  // if loan is bigger the 0, tax when transferring money direct tot bank 10 percent goes to loan
  if (loanAmount > 0) {
    currentTransfer = parseInt(tax * 9);
    loanAmount = parseInt(loanAmount) - parseInt(tax);
    loanBalance.innerHTML = parseInt(loanAmount);
    balance = parseInt(balance) + parseInt(currentTransfer);
  } else {
    balance = parseInt(balance) + pay;
  }
  //if loan is smaller 0
  if (loanAmount < 0) {
    balance = balance - loanAmount;
    pay = 0;
    loanAmount = 0;
    loanBalance.innerHTML = loanAmount;
  }
  balanceElement.innerHTML = balance;
  pay = 0;
  payElement.innerHTML = pay;
};

//LOAN
//repay
const handleRepayLoan = () => {
  if (pay > 0) {
    loanAmount = parseInt(loanAmount) - pay;
    console.log(loanAmount);
    if (loanAmount <= 0) {
      pay = -parseInt(loanAmount);
      loanAmount = 0;
      repayElement.style.visibility = "hidden";
    }
    //remaing pay goes to balance and we reset pay
    balance = parseInt(balance) + pay;
    loanBalance.innerHTML = loanAmount;
    balanceElement.innerHTML = balance;
    pay = 0;
    payElement.innerHTML = pay;
  }
};

//laptops
const addLaptopsToMenu = (laptops) => {
  laptops.forEach((x) => addLaptopToMenu(x));
  //show first element on page features
  for (const x of laptops[0].specs) {
    const specsItem = document.createElement("li");
    specsItem.innerHTML = `${x}`;
    featuresElement.appendChild(specsItem);
  }
  //template literals for the first item
  const footerImage =
    "https://noroff-komputer-store-api.herokuapp.com/assets/images/1.png";

  const markup = `
  <div class="col-md-4 bg-light" style="width: w-100;">
  <div class="card h-100  bg-light" style="width: w-100;">
      <img src="${footerImage}" class="card-img-top img-responsive w-100 h-100"  alt="img">
  </div>
</div>
<div class="col-md-4 ">
  <div class="card bg-light h-100 border-0 justify-content-center" style="width: w-100;">
      <h3 class="card-title fs-1 text-center mb-5 mt-3">${laptops[0].title}</h3>
      <p class="card-title fs-3 text-center">${laptops[0].description}</p>
  
  </div>
</div>
<div class="col-md-4">
  <div class="card bg-light h-100 border-0 justify-content-center" style="width: w-100;">
      <h3 class="card-title fs-1 text-center mb-5 mt-3">${laptops[0].price} NOK</h3>
      <button class="  btn btn-info w-50 buy mx-auto" id="buy"  onclick="handleBuyLaptop()">Buy Now</button>
  </div>
</div>
  `;
  footerElement.innerHTML = markup;
};

//creating option to and appending title
const addLaptopToMenu = (laptop) => {
  const { id, title } = laptop;
  const laptopElement = document.createElement("option");
  laptopElement.value = id;
  laptopElement.appendChild(document.createTextNode(title));
  laptopsElement.appendChild(laptopElement);
};

const handleLaptopChangeMenu = () => {
  //resets
  featuresElement.innerHTML = "";
  //the selected index
  const selectedLaptop = laptops[laptopsElement.selectedIndex];
  //features looping
  for (const x of selectedLaptop.specs) {
    const specsItem = document.createElement("li");
    specsItem.innerHTML = `${x}`;
    featuresElement.appendChild(specsItem);
  }
  handleFooterSection(selectedLaptop);
};

//handle footer section, markup for the footer" argument is the selectedLaptop from handleLaptopChangeMenu"
const handleFooterSection = (x) => {
  const { title, description, price, image } = x;
  let footerImage = `https://noroff-komputer-store-api.herokuapp.com/${image}`;
  let newFooterImage = footerImage.replace("jpg", "png");
  let markup;
  //for the img number 5, we replace to get the png
  if (footerImage.includes(5)) {
    markup = `
    <div class="col-md-4 bg-light" style="width: w-100;  " >
    <div class="card h-100  bg-light" style="width: w-100;">
        <img src="${newFooterImage}"  class="card-img-top img-responsive w-100 h-100"  alt="img">
    </div>
  </div>
  <div class="col-md-4  bg-light">
    <div class="card h-100 border-0 justify-content-center bg-light " style="width: w-100;">
        <h3 class="card-title  fs-1 text-center mb-5 mt-3">${title}</h3>
        <p class="card-title fs-3 text-center">${description}</p>
    </div>
  </div>
  <div class="col-md-4  bg-light">
  <div class="card h-100 border-0 justify-content-center bg-light" style="width: w-100;">
     <h3 class="card-title  text-center mb-5 mt-3">${price} NOK</h3>
        <button class=" btn btn-info  w-50 buy mx-auto" id="buy" onclick="handleBuyLaptop()">Buy Now</button>
    </div>
  </div>
      `;
  } else {
    markup = `
    <div class="col-md-4  bg-light" style="width: w-100;" >
    <div class="card h-100  bg-light" style="width: w-100;">
        <img src="${footerImage}"  class="card-img-top img-responsive w-100 h-100"  alt="img">
    </div>
  </div>
  <div class="col-md-4  bg-light">
    <div class="card h-100 border-0 justify-content-center  bg-light" style="width: w-100;">
        <h3 class="card-title  fs-1 text-center mb-5 mt-3">${title}</h3>
        <p class="card-title fs-3 text-center">${description}</p>
    
    </div>
  </div>
  <div class="col-md-4  bg-light">
  <div class="card h-100 border-0 justify-content-center  bg-light" style="width: w-100;">
     <h3 class="card-title  text-center mb-5 mt-3">${price} NOK</h3>
        <button class=" btn btn-info  w-50 buy mx-auto" id="buy" onclick="handleBuyLaptop()">Buy Now</button>
    </div>
  </div>
  `;
  }
  footerElement.innerHTML = markup;
};

//handle buy button, using window to access the onclick in the markup
window.handleBuyLaptop = () => {
  //the selected laptops id
  const selectedLaptop = laptops[laptopsElement.selectedIndex];
  //price
  let priceTag = selectedLaptop.price;
  //checking if we have enough money
  if (balance >= priceTag) {
    console.log("you have money to purchase the item");
    console.log(balance);
    let newBalance = parseInt(balance) - priceTag;
    balanceElement.innerHTML = parseInt(balance) - priceTag;
    balance = newBalance;
    console.log(balance);
    alert(`You are a owner of a ${selectedLaptop.title}`);
  } else {
    alert("You do not have enough money in the Bank");
  }
};
//eventListeners
laptopsElement.addEventListener("change", handleLaptopChangeMenu);
loanElement.addEventListener("click", handleLoan);
workElement.addEventListener("click", handleWork);
bankElement.addEventListener("click", handleTransfersMoney);
repayElement.addEventListener("click", handleRepayLoan);
